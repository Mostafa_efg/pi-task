import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor() {}
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<any> {
    return next.handle(request).pipe(
      catchError((error: HttpErrorResponse) => {
        switch (error.status) {
          case 404:
            // TO DO Handle Any Logic Here
            break;
          case 422:
          case 490:
            // TO DO Handle Any Logic Here
            break;
          case 492:
            // TO DO Handle Any Logic Here
            break;
          default:
            console.log(error);
            break;
        }

        return throwError(error);
      })
    );
  }
}
