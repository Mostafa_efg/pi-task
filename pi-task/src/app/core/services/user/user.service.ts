import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ResponseModel } from '../../models/response-model';

@Injectable()
export class UserService {
  constructor(private http: HttpClient) {}

  getUsers(): Observable<ResponseModel> {
    return this.http.get<ResponseModel>(`${environment.API_BASE_URL}users`);
  }
}
