import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { ResponseModel } from 'src/app/core/models/response-model';
import { UsersModel } from 'src/app/core/models/users-model';
import { UserService } from 'src/app/core/services/user/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit, AfterViewInit {
  users: Array<UsersModel> = new Array<UsersModel>();
  clonedUsers: Array<UsersModel> = new Array<UsersModel>();

  userInput: Subject<string> = new Subject<string>();
  @ViewChild('searchInput', { static: true }) SearchInput: any;

  // MatPaginator Variables
  length = 0;
  pageSize = 2;
  pageSizeOptions = [2, 4, 6];

  constructor(
    private userService: UserService,
    private spinner: NgxSpinnerService
  ) {}

  ngAfterViewInit() {
    // This calls when user seach by name we wait for a half of second and calling the search function.
    this.userInput.pipe(debounceTime(500)).subscribe((name: string) => {
      this.onSearchUser(name);
    });
  }

  ngOnInit(): void {
    this.spinner.show();
    this.getAllUsers();
  }

  //#region Listing Users

  getAllUsers() {
    this.userService.getUsers().subscribe(
      (response: ResponseModel) => {
        if (response && response.data && response.data.length) {
          this.users = response.data;
          this.clonedUsers = Object.assign([], this.users);
          this.length = this.users.length;
          this.pageSize = this.users.length;
        } else {
          this.resetUsersList();
        }
        setTimeout(() => {
          this.spinner.hide();
        }, 500);
      },
      () => {
        console.log('Error Occured!');
        this.resetUsersList();
        this.spinner.hide();
      }
    );
  }

  resetUsersList() {
    this.users = this.users.splice(this.users.length);
    this.length = 0;
  }

  //#endregion

  //#region Search User

  onUserNameChange(name: string) {
    this.userInput.next(name);
  }

  // Please note that i made the search form front end side
  // because the mentioned api doesn't support filtering by user name
  onSearchUser(name: string) {
    this.showHideSpinner();

    if (name) {
      this.clonedUsers = this.users.filter((user: UsersModel) =>
        user.first_name.toLowerCase().includes(name.toLowerCase())
      );
    } else {
      this.clonedUsers = Object.assign([], this.users);
    }
  }

  //#endregion

  //#region Pagination

  handlePageEvent(event: PageEvent) {
    this.showHideSpinner();

    if (this.SearchInput.nativeElement.value)
      this.SearchInput.nativeElement.value = null;

    this.length = event.length;
    this.pageSize = event.pageSize;

    this.clonedUsers = this.users
      .slice(0, this.pageSize)
      .map((user: UsersModel) => user);
  }

  //#endregion

  //#region Drag And Drop Cards

  onDragUser(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.clonedUsers, event.previousIndex, event.currentIndex);
  }

  //#endregion

  //#region Spinner

  showHideSpinner() {
    this.spinner.show();

    setTimeout(() => {
      this.spinner.hide();
    }, 500);
  }

  //#endregion
}
